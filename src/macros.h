/*
 * macros.h
 *
 *  Created on: May 13, 2022
 *      Author: zamek
 */

#ifndef MACROS_H_
#define MACROS_H_

#define MALLOC(size,ptr)  \
	do { 				  \
		ptr=malloc(size); \
		if (!ptr) {		  \
			syslog(LOG_EMERG, "Malloc failed"); \
			abort();							\
		}										\
	} while(0)


#define FREE(ptr) \
	do { 			\
		free(ptr);	\
		ptr=NULL;	\
	} while(0)


#endif /* MACROS_H_ */
