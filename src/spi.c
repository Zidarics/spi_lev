/*
 * spi.c
 *
 *  Created on: May 13, 2022
 *      Author: zamek
 */

#include <stdlib.h>
#include <syslog.h>

#include "spi.h"
#include "macros.h"

#define MAX_POOL_SIZE 42

static TAILQ_HEAD(head, package) spi_list=TAILQ_HEAD_INITIALIZER(spi_list);

static TAILQ_HEAD(p_head, package) pool=TAILQ_HEAD_INITIALIZER(pool);

static bool initialized = false;

int spi_init(uint8_t size){
	syslog(LOG_DEBUG, "Enter spi_init, size:%d", size);

	if (size>MAX_POOL_SIZE)
		return EXIT_FAILURE;

	initialized=true;

	for(int i=0;i<size;++i) {
		package_t *p;
		MALLOC(sizeof(package_t), p);
		p->channel=0;
		p->value=0;
		p->valid=false;
		spi_free_package(p);
	}
	return EXIT_SUCCESS;
}

int spi_deinit(){
	syslog(LOG_DEBUG, "Enter spi_deinit");
	if (!initialized) {
		syslog(LOG_WARNING, "Deinit called but no init happened");
		return EXIT_FAILURE;
	}

	while(! TAILQ_EMPTY(&spi_list)) {
		package_t *p=TAILQ_FIRST(&spi_list);
		if (!p) {
			TAILQ_REMOVE(&spi_list, p, next);
			FREE(p);
		}
	}

	while(! TAILQ_EMPTY(&pool)) {
		package_t *p=TAILQ_FIRST(&pool);
		if (!p) {
			TAILQ_REMOVE(&pool, p, next);
			FREE(p);
		}
	}
	initialized=false;
	return EXIT_SUCCESS;
}

int spi_add_package(uint8_t channel, uint16_t value){
	syslog(LOG_DEBUG, "Enter spi_add_package, ch:%d, v:%d", channel, value);
	if (!initialized) {
		syslog(LOG_WARNING, "spi_add_package but not initialized");
		return EXIT_FAILURE;
	}

	if (! (channel<=SPI_MAX_CHANNEL && value<=SPI_MAX_CHANNEL_VALUE)) {
		syslog(LOG_WARNING, "channel or value is invalid");
		return EXIT_FAILURE;
	}

	package_t *p=TAILQ_FIRST(&pool);
	if (!p) {
		syslog(LOG_WARNING, "pool is too small");
		return EXIT_FAILURE;
	}

	TAILQ_REMOVE(&pool, p, next);
	p->channel=channel;
	p->value=value;
	p->valid=true;
	TAILQ_INSERT_HEAD(&spi_list, p, next);
	return EXIT_SUCCESS;
}

bool spi_is_package(){
	return initialized && ! TAILQ_EMPTY(&spi_list);
}

package_t *spi_get_package(){
	syslog(LOG_DEBUG, "Enter get_package");
	if (!initialized || TAILQ_EMPTY(&spi_list)) {
		syslog(LOG_WARNING, "spi_get_package list is empty or not initialied");
		return NULL;
	}

	package_t *p=TAILQ_LAST(&spi_list, head);
	if (! (p && p->valid)) {
		syslog(LOG_ERR, "spi_get_package valid is false or p is NULL");
		return NULL;
	}

	TAILQ_REMOVE(&spi_list, p, next);
	return p;
}

int spi_free_package(package_t *pkg){
	syslog(LOG_DEBUG, "Enter spi_free_package");
	if (! (initialized && pkg)) {
		syslog(LOG_WARNING, "spi_free_package pkg is null or not initialized");
		return EXIT_FAILURE;
	}

	pkg->valid=false;
	TAILQ_INSERT_HEAD(&pool, pkg, next);
	return EXIT_SUCCESS;
}



