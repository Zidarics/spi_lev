/*
 * spi.h
 *
 *  Created on: May 13, 2022
 *      Author: zamek
 */

#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>
#include <stdbool.h>
#include <sys/queue.h>


#define SPI_MAX_CHANNEL 8
#define SPI_MAX_CHANNEL_VALUE 4096

typedef struct package {
	bool valid;
	uint8_t channel;
	uint16_t value;
	TAILQ_ENTRY(package) next;
} package_t;

int spi_init(uint8_t size);

int spi_deinit();

int spi_add_package(uint8_t channel, uint16_t value);

bool spi_is_package();

package_t *spi_get_package();

int spi_free_package(package_t *pkg);


#endif /* SPI_H_ */
