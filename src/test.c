/*
 * test.c
 *
 *  Created on: May 13, 2022
 *      Author: zamek
 */


#include <stdlib.h>
#include <syslog.h>
#include <CUnit/Basic.h>

#include "spi.h"

#define POOL_SIZE 16

int before_without_init() {
	return 0;
}

int after_without_init() {
	return 0;
}

int before_with_init() {
	return spi_init(POOL_SIZE);
}

int after_with_init() {
	return spi_deinit();
}

void test_without_init() {
	CU_ASSERT_EQUAL(spi_add_package(0, 42), EXIT_FAILURE);
	CU_ASSERT_FALSE(spi_is_package());
	CU_ASSERT_PTR_NULL(spi_get_package());
	CU_ASSERT_EQUAL(spi_free_package(NULL), EXIT_FAILURE);
	//TODO more tests for uninitialized
	CU_PASS("test_without_init passed");
}

void test_with_init() {
	CU_ASSERT_FALSE(spi_is_package());
	CU_ASSERT_PTR_NULL(spi_get_package());

	CU_ASSERT_EQUAL(spi_add_package(0, 42), EXIT_SUCCESS);
	CU_ASSERT_TRUE(spi_is_package());
	package_t *p=spi_get_package();
	CU_ASSERT_PTR_NOT_NULL(p);
	CU_ASSERT_EQUAL(p->channel, 0);
	CU_ASSERT_EQUAL(p->value, 42);
	CU_ASSERT_TRUE(p->valid);

	CU_ASSERT_EQUAL(spi_free_package(p), EXIT_SUCCESS);

	CU_ASSERT_FALSE(spi_is_package());
	CU_ASSERT_PTR_NULL(spi_get_package());

	for (int i=0;i<POOL_SIZE;++i) {
		CU_ASSERT_EQUAL(spi_add_package(i%SPI_MAX_CHANNEL, i+100), EXIT_SUCCESS);
	}

	CU_ASSERT_TRUE(spi_is_package());
	CU_ASSERT_EQUAL(spi_add_package(0, 42), EXIT_FAILURE);

	for (int i=0;i<POOL_SIZE;++i) {
		//TODO check items
	}

	//TODO is_package();
	//TODO get_package()
	CU_PASS("with init passed");
}


int main(int argc, char **argv) {
	openlog("spi_queue", LOG_CONS | LOG_PID | LOG_NDELAY,
			LOG_LOCAL1);

	if(CUE_SUCCESS!=CU_initialize_registry()) {
		syslog(LOG_ERR, "CU_initialize_registry() returns with %d",
				CU_get_error());
		goto error_exit;
	}

	CU_pSuite without_init=NULL;
	CU_pSuite with_init=NULL;
	without_init=CU_add_suite("without init", before_without_init,
							 after_without_init);
	if (!without_init) {
		syslog(LOG_ERR, "without_init suit create failed");
		goto error_exit;
	}

	if (!CU_add_test(without_init, "test without init", test_without_init)) {
		syslog(LOG_ERR, "cannot add test for without_init");
		goto error_exit;
	}

	with_init=CU_add_suite("with init", before_with_init,
							 after_with_init);

	if (!with_init) {
		syslog(LOG_ERR, "with_init suit create failed");
		goto error_exit;
	}

	//TODO test function

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();

error_exit:
	closelog();
}


