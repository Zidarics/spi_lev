#Makefile for SPI

DEBUG=y

CFLAGS=-Wall -std=c99 -pedantic

ifeq ($(DEBUG),y)
CFLAGS+=-g
endif

OBJS=$(patsubst src/%.c,bin/%.o, $(wildcard src/*.c))

all: bin/spi_queue

bin/spi_queue: bin $(OBJS)
		$(CC) -o $@ $(OBJS) $(CFLAGS) -l cunit
		$@
		
bin:
		mkdir bin
		
bin/%.o: src/%.c
		$(CC) -c $< -o $@ $(CFLAGS)
		
clean:
		$(RM) -r bin
		
.PHONY: clean


